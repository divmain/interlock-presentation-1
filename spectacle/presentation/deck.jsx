import React from "react/addons";

const jsonLoaderExample = `
export default function (opts={}) {
  const isJsonFile = opts.filter || /\.json$/;

  return (override, transform) => {
    transform("readSource", function (source, args) {
      const [asset] = args;
      if (isJsonFile.test(asset.path)) {
        source = \`module.exports = \${source};\`;
      }
      return source;
    });
  };
};
`.slice(1);

import {
  Appear, BlockQuote, Cite, CodePane, Deck, Fill, Fit,
  Heading, Image, Layout, Link, ListItem, List, Quote, Slide, Text
} from "../src/spectacle";

import preloader from "../src/utils/preloader";

const images = {
  gitGraph: require("./git-graph.png"),
  jsGraph: require("./js-graph.png"),
  // h1graph: require("./http1-graph.png"),
  // h1details: require("./http1-details.png"),
  // h2graph: require("./http2-graph.png"),
  // h2details: require("./http2-details.png"),
  readSourceDoc: require("./readsource-doc.png"),
  logo: require("./interlock-plain.png"),
  cacheGraph: require("./cache-graph.png"),
  bundlerPrimer: require("./js-bundler-primer.png")
};

preloader([images.city, images.kat]);

const Item = React.createClass({
  getDefaultProps() {
    return {
      inline: true,
      margin: "0"
    };
  },
  propTypes: {
    inline: React.PropTypes.boolean,
    children: React.PropTypes.node,
    fid: React.PropTypes.string
  },
  render() {
    return <Appear inline={this.props.inline} fid={this.props.fid} fontSize="3em" margin={this.props.margin}>{this.props.children}</Appear>;
  }
});

export default class extends React.Component {
  render() {
    return (
      <Deck transition={["zoom", "slide"]} transitionDuration={800}>
        <Slide transition={["slide"]}>
          <Heading size={1} fit textColor="black">
            Interlock.js
          </Heading>
          <Heading size={2} fit>
            inspired by Git - built on Babel
          </Heading>
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={1} fit textColor="black">
            What is it?
          </Heading>
          <Appear fid="2">
            <Heading size={2} caps fit textColor="black">
              a superset of Webpack & RequireJS
            </Heading>
          </Appear>
          <Appear fid="3">
            <Heading size={2} caps fit textColor="black">
              reconsidered at every level
            </Heading>
          </Appear>
          <Appear fid="4">
            <Heading size={2} caps fit textColor="black">
              ambitious
            </Heading>
          </Appear>
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.bundlerPrimer.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={1} fit textColor="black">
            Webpack
          </Heading>
          <Appear fid="2">
            <Heading size={2} caps fit textColor="black">
              three primary weaknesses
            </Heading>
          </Appear>
          <Appear fid="3">
            <Heading size={2} caps fit textColor="black">
              module system
            </Heading>
          </Appear>
          <Appear fid="4">
            <Heading size={2} caps fit textColor="black">
              intermediate representation
            </Heading>
          </Appear>
          <Appear fid="5">
            <Heading size={2} caps fit textColor="black">
              it's codebase ❮/troll❯
            </Heading>
          </Appear>
        </Slide>


        <Slide transition={["slide"]}>
          <Appear fid="1">
            <Heading size={2} caps textColor="black" textSize="3em">
              Beyond solving the seminal problems<br />
              that prompted this endeavor,<br />
              Interlock should
            </Heading>
          </Appear>
          <div style={{ marginTop: "20px" }}/>
          <Item fid="2">be readable</Item>
          <Item fid="3"> • be correct</Item>
          <Item fid="4"> • provide excellent developer ergonomics</Item>
          <Item fid="5"> • be modular</Item>
          <Item fid="6"> • be familiar</Item>
          <div style={{ marginTop: "20px" }}/>
          <Item inline={false}>These qualities will make the difference between shipping something that is interesting and shipping something that is eminently useful.</Item>
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={2} caps fit textColor="black">
            Other goals
          </Heading>
          <Appear fid="1">
            <Heading size={2} caps fit textColor="black">
              Compatible with best-in-class tools
            </Heading>
          </Appear>
          <Item fid="2">Babel</Item>
          <Item fid="3"> • ESlint</Item>
          <Item fid="4"> • escodegen</Item>
          <Item fid="5"> • UglifyJS</Item>
          <Item fid="6"> • esprima</Item>
          <Appear fid="7">
            <Heading size={2} caps fit textColor="black">
              Sane plugin system
            </Heading>
          </Appear>
          <Item fid="8">isolated state</Item>
          <Item fid="9"> • clear & up-to-date documentation</Item>
          <Item fid="10"> • minimal constraints</Item>
          <Appear fid="11">
            <Heading size={2} caps fit textColor="black">
              Much more robust module system
            </Heading>
          </Appear>
          <Item fid="12">multiple builds & shared code</Item>
          <Item fid="13"> • canonical module IDs</Item>
          <Item fid="14"> • inspired by Git!</Item>
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.gitGraph.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.jsGraph.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={1} fit textColor="black">
            Why do canonical IDs matter?
          </Heading>
          <Item fid="1" inline={false}> • multiple builds don't conflict</Item>
          <Item fid="2" inline={false}> • same chunk of code need never be downloaded twice</Item>
          <Item fid="3" inline={false}> • bundles can be easily shared between teams & builds</Item>
          <Item fid="4" inline={false}> • cache busting is no longer necessary</Item>
          <Item fid="5" inline={false}> • robust and predictable dependency graphs</Item>
          <Item fid="6" inline={false}> • enables creative extensions, like</Item>
          <Item fid="7" inline={false}>community CDN integration</Item>
          <Item fid="8" inline={false}>in-browser cache</Item>
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.cacheGraph.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

{/*
        <Slide transition={["slide"]}>
          <Heading size={1} Fit>
            HTTP/1.1
          </Heading>
          <Heading size={2} textSize="3em" Fit>
            vs
          </Heading>
          <Heading size={1} Fit>
            HTTP/2
          </Heading>
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.h1graph.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.h1details.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.h2graph.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.h2details.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>
*/}
        <Slide transition={["slide"]}>
          <Heading size={2}>Design</Heading>
          <div style={{ marginTop: "20px" }}/>
          <Item fid="1">pure functions</Item>
          <Item fid="2"> • constituent pieces behave like React components w/ isolated state</Item>
          <div style={{ marginTop: "20px" }}/>
          <Item fid="3">pluggable</Item>
          <Item fid="4"> • every stage of compilation can be overridden or transformed</Item>
          <div style={{ marginTop: "20px" }}/>
          <Item fid="5">developer-friendly</Item>
          <Item fid="6"> • auto-gen'd documentation, correctness enforced by CI</Item>
          <div style={{ marginTop: "20px" }}/>
          <Item fid="7">AST over strings</Item>
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={2} fit>Example - JSON Plugin</Heading>
          <CodePane
            lang="javascript"
            source={jsonLoaderExample}
            margin="20px auto"
            textSize="1.5em" />
          <Text fit textColor="black">
            https://github.com/interlockjs/interlock/blob/master/docs/extensibility.md#readsource
          </Text>
        </Slide>

        <Slide transition={["slide"]}>
          <Image src={images.readSourceDoc.replace("/", "")} margin="0px auto 40px" width="100%" />
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={2}>
            Completed
          </Heading>
          <Item fid="1">code splitting</Item>
          <Item fid="2"> • module system</Item>
          <Item fid="3"> • partial recompile (watch)</Item>
          <Item fid="4"> • sourcemaps</Item>
          <Item fid="5"> • first-class multi-build support</Item>
          {/*<Item fid="6"> • HTTP/1.1 profile</Item>*/}
          <Item fid="7"> • AST as intermediate data-structure</Item>
          <Item fid="8"> • ES2015+ and JSX first-class support</Item>
          <Item fid="9"> • CommonJS, AMD, ES2015 modules</Item>
          <Item fid="10"> • fully async, concurrent compilations</Item>
          <Appear fid="11">
            <Heading size={2} textSize="4em" margin="20px 0 0">
              Plugins/Loaders
            </Heading>
          </Appear>
          <Item fid="12">handlebars</Item>
          <Item fid="13"> • json</Item>
          <Item fid="13"> • uglify</Item>
          <Item fid="14"> • esmangle</Item>
          <Item fid="14"> • localstorage!</Item>
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={2}>
            Soon
          </Heading>
          <Item fid="1">async requires</Item>
          <Item fid="2"> • IndexedDB module caching</Item>
          <Item fid="3"> • drop-in support for Babel plugins</Item>
          {/*<Item fid="4"> • sourcemap URL plugin</Item>*/}
          <Item fid="5"> • dev server with hot reloading</Item>
          <Item fid="6"> • css plugins</Item>
          <Item fid="7"> • helpful errors everywhere</Item>
        </Slide>

        <Slide transition={["slide"]}>
          <Heading size={2}>
            Still to come
          </Heading>
          <Item fid="1">HTTP/2.0 profile</Item>
          <Item fid="2"> • hybrid H1/H2 loader</Item>
          <Item fid="3"> • alternate pipeline for non-JS</Item>
          <Item fid="4"> • input sourcemaps (for compile-to-JS languages)</Item>
          <Item fid="5"> • complete code coverage</Item>
          <Item fid="6"> • comprehensive documentation</Item>
          <Item fid="7"> • website</Item>
        </Slide>


        <Slide transition={["slide"]} maxHeight="auto">
          <Appear fid="1">
            <Image src={images.logo.replace("/", "")} margin="0px auto 40px" maxHeight="60vh" />
          </Appear>
        </Slide>

      </Deck>
    );
  }
}
